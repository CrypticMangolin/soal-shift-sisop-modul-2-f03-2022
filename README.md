# Soal Shift Sisop Modul 2 F03 2022

## Anggota Kelompok

| Nama                       | NRP        |
| :------------------------- | :--------- |
| Hidayatullah               | 5025201031 |
| Muhammad Daffa Aldriantama | 5025201177 |
| Atha Dzaky Hidayanto       | 5025201269 |

---
## Soal 2
### Cara Pengerjaan
a. Pada soal a diminta untuk meng-*extract* file zip drakor yang diberikan, 
dan hanya disimpan file poster yang berupa png.

Bagian pertama dapat dilakukan dengan membuat 2 process baru untuk menyiapkan directory tujuan 
dan process kedua akan meng-*unzip* file zip ke folder tujuan. 
```c++
int main() {
    char FolderDir[] = "/home/nao/drakor";
    int status_Unzip;
    pid_t child_unzip = fork();

    if (child_unzip == 0) {
    	int status_mkdir;
    	pid_t child_mkdir = fork();

        if (child_mkdir==0) MKDir(FolderDir);
        else {
            while ((wait(&status_mkdir)) > 0);
            UNZip("drakor.zip", FolderDir);
        }
    }
```
Pemanggilan semua `exec` dibuatkan suatu fungsi general agar dapat digunakan kembali nantinya.
```c++
void MKDir(char *folder) {
    char *argv[] = {"mkdir", "-p", folder, NULL};
    execv("/bin/mkdir", argv);
}
    
void UNZip(char *zipfile, char *folder) {
    char *argv[] = {"unzip", zipfile, "-d", folder, NULL};
    execv("/bin/unzip", argv);
}
```
Pembersihan file irrelevan dilakukan setelah proses unzip selesai dengan membuat array nama file dalam folder drakor.
Lalu, menghapusnya menggunakan `rm`.
```c++
} else {
        while ((wait(&status_Unzip)) > 0);
        
        int statusPP;
        pid_t child_PosterProcess = fork();

        if(child_PosterProcess == 0) {
            char **filelist;
            size_t fcount;
            fcount = file_list(FolderDir, &filelist, 0);
```
array filelist akan berisi nama file/folder dalam folder. Fungsi `file_list` akan memiliki 3 parameter,
folder yang isinya mau didata, array untuk menyimpan nama file/folder, dan mode. Mode 1 akan mendata semua nama file/folder, mode selain 1 hanya akan mendata .png dan menghapus sisanya.
```c++
void RM_r(char *filedir) {
    char *argv[] = {"rm", "-r", filedir, NULL};
    execv("/bin/rm", argv);
}

size_t file_list(const char *path, char ***ls, int mode) {
    // Array file listing
    size_t count = 0;
    DIR *dp = NULL;
    struct dirent *ep = NULL;

    dp = opendir(path);
    if(NULL == dp) {
        fprintf(stderr, "no such directory: '%s'", path);
        return 0;
    }

    *ls = NULL;
    ep = readdir(dp);
    while(NULL != ep){
        count++; 
        ep = readdir(dp);
    }

    rewinddir(dp);
    *ls = calloc(count, sizeof(char *));

    count = 0;
    ep = readdir(dp);
    while(NULL != ep) {
        // Skip . and ..
        if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
            if (mode == 1) {
                (*ls)[count++] = strdup(ep->d_name);
            } else {
                if (strcmp(strrchr(ep->d_name, '\0') - 4, ".png") == 0) {
                    (*ls)[count++] = strdup(ep->d_name);
                } else {
                    // REMOVE DIR
                    if (fork() == 0) {
                        char filedir[50] = "drakor/";
                        strcat(filedir, ep->d_name);
                        RM_r(filedir);
                    }
                }
            }
        }
        ep = readdir(dp);
    }

    closedir(dp);
    return count;
}
```
fungsi `file_list` akan melalui direktori 2 kali, yang pertama untuk menghitung jumlah file dalam folder. 
Hasilnya akan digunakan untuk mengalokasikan memori untuk array. Kedua untuk memasukkan nama file kedalam array, 
`strdup` akan me-return pointer dari string baru yang dibuat. `strcmp(strrchr(ep->d_name, '\0') - 4, ".png") == 0` 
digunakan untuk mencocokkan 4 character terakhir dengan `.png`. Jika tidak cocok, file/folder tersebut akan dihapus
secara rekursif menggunakan `rm -r`.

b. Membuat direktori untuk setiap genre yang ada.

Genre suatu film dalam poster tertulis dalam nama file poster, namun ada kemungkinan suatu poster
memfiturkan lebih dari satu film dengan genre berbeda. Tiap film dalam poster perlu dipisahkan terlebih dahulu.
```c++
for(size_t i=0; i<fcount; i++) {            
    char curFile[60];
    strcpy(curFile, filelist[i]);

    filelist[i][strlen(filelist[i])-4]='\0';

    char **PosterArr = NULL;
    size_t PArrSize = strSplit(filelist[i], "_", &PosterArr);
```
Untuk setiap file yang sudah didata dalam array `filelist`, nama filenya akan disimpan dahulu dalam `curfile` karena 
string nama filenya akan diolah dan nama file aslinya nantinya masih dibutuhkan untuk process copy. Nama file akan dihapus extensinya agar lebih mudah diproses dengan mengganti karakter ke-empat dari belakang dengan `NULL`. 

Setelah itu, fungsi `strSplit` akan mendata semua nama poster kedalam `PosterArr`. Dengan menggunakan strtok untuk mengambil substring yang dipisahkan oleh delimiter. Untuk tiap token yang berhasil diambil, memori array akan di realokasikan agar dapat memuat token baru tersebut.
```c++
size_t strSplit (char *str, char *delim, char ***array) {
    size_t i = 0;  
    char **tempArr = NULL;
    char * token = strtok(str, delim);
    while( token != NULL ) {
        tempArr = (char **)realloc(tempArr, (i + 1) * sizeof(char *));
        tempArr[i++] = token;
        token = strtok(NULL, delim);
    }
    *array = tempArr;
    return i;
}
```
Untuk setiap poster, akan dioperasikan kembali dengan cara yang serupa untuk memisahkan tiap atribut dalam poster tersebut. Yaitu, nama, tahun, dan genre. `sprintf` akan membuat path yang nantinya akan dipakai untuk membuat folder suatu genre. Setelah itu, dibuat folder baru untuk genre tersebut (jika sudah ada, ywd gpp :)).
```c++
int statusGmk;
pid_t child_G_mkdir;
for (size_t j=0; j<PArrSize; j++) {                
    char temp[50];
    strcpy(temp,PosterArr[j]);
    char **Poster = NULL;
    size_t PSize = strSplit(temp, ";", &Poster);
                    
    char genreDir[100];
    sprintf(genreDir, "%s/%s", FolderDir, Poster[2]);
                    
    child_G_mkdir = fork();                
        
    if (child_G_mkdir==0) {
        MKDir(genreDir);
```
c-d. memindahkan tiap gambar ke folder genre masing-masing.

Dalam satu gambar ada kemungkinan harus dipindahkan ke dua genre yang berbeda. Sehingga lebih cocok jika menggunakan copy. Setelah gambar di copy ke semua genre yang terkait, file aslinya dihapus.
```c++
    } else {
        while ((wait(&statusGmk)) > 0);
        pid_t child_cp = fork();

        if (child_cp == 0) {
            char src[100], dst[100];
            sprintf(src, "%s/%s", FolderDir, curFile);
            sprintf(dst, "%s/%s/%s.png", FolderDir, Poster[2], Poster[0]);
            CPY(src, dst);
        }
    }
}
if (fork() == 0) {
    char a[100];
    sprintf(a, "%s/%s", FolderDir, curFile);
    RM_r(a);
}
```
Untuk fungsi `CPY` nya.
```c++
void CPY(char *srcFile, char *Destination) {
    char *argv[] = {"cp", srcFile, Destination, NULL};
    execv("/bin/cp", argv);
}
```
e. Untuk setiap folder genre ada file `data.txt` yang berisi nama dan tahun rilis film yang ada dalam suatu genre tertentu. Penulisan diurutkan sesuai tahun rilis.

Untuk masalah ini, dipecah menjadi 2 bagian, yang pertama pendataan isi folder tiap genre, lalu bagian sort. Bagian pertama dilakukan bersamaan dengan pemrosesan gambar. Setelah gambar di copy pada genre sesuai, akan didata pada file `data.txt` dalam folder genre tersebut dengan format jumlah film pada baris pertama, diikuti n baris berisi tahun dan nama film dipisahkan spasi. Contoh isi
```
3
2015 who are you
2021 school2021
2020 true beauty
```
```c++
FILE *printPtr, *sizePtr;
char datafile[100];
sprintf(datafile, "%s/%s/data.txt", FolderDir, Poster[2]);
printPtr = fopen(datafile, "a");
sizePtr = fopen(datafile, "r+");
int pcount;
if (fscanf(sizePtr, "%d", &pcount) == 1)
{
    rewind(sizePtr);
    pcount++;
    fprintf(sizePtr, "%d\n", pcount);
}
else
{
    fprintf(sizePtr, "1\n");
}
fprintf(printPtr, "%s %s\n", Poster[1], Poster[0]);
fclose(sizePtr);
fclose(printPtr);
```
Ditulis dengan menggunakan 2 pointer file, satu untuk menscan jumlah film, dan mengupdatenya, kedua untuk menulis tahun dan nama filmnya.

Setelah semua file telah di proses dan dirapihkan. Tiap data.txt dalam tiap genre akan dibaca, diurutkan, dan diprint sesuai formatnya. Pada bagian ini menggunakan struct Film untuk menyimpan tahun dan nama dari film.
```c++
sleep(1);
while ((wait(&statusPP)) > 0);
char **folderlist;
size_t count;

count = file_list(FolderDir, &folderlist, 1);
for (int i = 0; i < count; i++) {
    int n;
    char datafile[100];
    sprintf(datafile, "%s/%s/data.txt", FolderDir, folderlist[i]);
    FILE *f = fopen(datafile, "r+");

    fscanf(f, "%d ", &n);
    Film Poster[n];
    for (int j = 0; j < n; j++) {
        fscanf(f, "%d %s ", &Poster[j].year, Poster[j].title);
    }

    qsort(Poster, n, sizeof(Film), comparator);
    rewind(f);

    fprintf(f, "Kategori : %s", folderlist[i]);
    for (int j = 0; j < n; j++) {
        fprintf(f, "\n\nNama : %s\nRilis Tahun : %d", Poster[j].title, Poster[j].year);
    }
    fclose(f);
}
```
Semua nama folder genre akan didata dalam array `folderlist`. `sleep(1)` digunakan untuk menunggu semua gambar dipindahkan dari folder drakor sehingga fungsi `file_list` hanya akan mendata folder genre. Setelah itu, untuk setiap folder genre, diakses `data.txt`, dan dibaca isinya. Nilai jumlah film digunakan untuk menginisialisasi size dari array Poster. Lalu, tahun dan nama dibaca. Setelah dibaca, array disort menggunakan qsort dan comparator yang membandingkan tahun (jika sama, nama film akan dibandingkan dengan `strcmp`). Setelah urut, data diprint sesuai format.
```c++
int comparator(const void *a, const void *b) 
{
    if (((Film *)a)->year == ((Film *)b)->year) {
        return strcmp(((Film *)a)->title, ((Film *)b)->title);
    }
    return (((Film *)a)->year-((Film *)b)->year);
}
```

### Kendala
- Kesalahan dalam mengakses atribut poster menyebabkan segmentation fault, sehingga gagal untuk membuat folder genre dan mengcopy gambar. Yang selanjutnya file aslinya dihapus karena dianggap sudah selesai diproses, yang menghasilkan folder drakor tidak ada isinya.

---

## Soal 3
### Penjelasan
Soal nomor 3 memerintahkan kita untuk mengekstrak file gambar dari zip yang telah disediakan. Setelah itu, gambar yang telah diekstrak tadi dipisahkan ke folder `air` dan `darat`. Terakhir, kita membuat `list.txt` dari gambar yang berada di folder `air`. 

### (a) Pembuatan Direktori air dan darat
Potongan program di bawah adalah digunakan untuk membuat direktori yang diminta, yaitu `air` dan `darat`.
```bash
void initDir() {
    pid_t child_id;
    child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) { 
            char *argv[] = {"mkdir", "/home/duffaldri/modul2/darat", NULL};
            execv("/bin/mkdir", argv);
    } else {
        while(wait(&status) > 0);
        pid_t child2_id;
        child2_id = fork();

        if(child2_id == 0) {
            sleep(3);
            char *argv[] = {"mkdir", "/home/duffaldri/modul2/air", NULL}; 
            execv("/bin/mkdir", argv);
        }
    }
}
```
Pertama, kita melakukan fork agar bisa melakukan exec `mkdir`. Lalu, karena dalam perintah kita diminta untuk memberikan jeda 3 detik di antara pembuatan direktori darat dan air, kita bisa menggunakan fungsi `sleep()` dengan parameter 3.

### (b) & (c) Ekstrak dan Memindahkan Gambar
Ekstrak dan pemisahan gambar dilakukan melakukan 2 fungsi, yaitu fungsi `unzipAnimal()` yang kemudian akan memanggil fungsi `moveAnimals()`.

Fungsi `unzipAnimal()`:
```bash
void unzipAnimal() {
    pid_t child_id = fork();
    int status;
    
    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"unzip", "/home/duffaldri/modul2/animal.zip", "-d", "/home/duffaldri/modul2/", NULL}; 
            execv("/usr/bin/unzip", argv);
    } else {
        while(wait(&status) > 0);
        moveAnimals();
    }  
}
```
Fungsi di atas akan melakukan unzip file `animal.zip`. Unzip dilakukan dengan menggunakan command bash `unzip`. Agar bisa dijalankan di C, kita bisa menggunakan fungsi `execv()`. Setelah selesai, fungsi `unzipAnimal()` akan memanggil fungsi `moveAnimals()`, seperti yang telah disebutkan di atas.

Fungsi `moveAnimals()`:
```bash
void moveAnimals() {
    pid_t child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if(child_id == 0) {
            char *argv[] = {"find", "/home/duffaldri/modul2/animal", "-iname", "*air*", "-exec", "cp", "{}", "/home/duffaldri/modul2/air/", ";", NULL};
            execv("/usr/bin/find", argv);
    } 
    else {
        while(wait(&status) > 0);
        pid_t child2_id = fork();
        
        if(child2_id < 0) exit(EXIT_FAILURE);
            
        if(child2_id == 0) {
            char *argv[] = {"find", "/home/duffaldri/modul2/animal", "-iname", "*darat*", "-exec", "cp", "{}", "/home/duffaldri/modul2/darat/", ";", NULL};
            execv("/usr/bin/find", argv);
        } else {
            while(wait(&status) > 0);
            pid_t child3_id = fork();

            if(child3_id < 0) exit(EXIT_FAILURE);

            if(child3_id == 0) {
                char *argv[] = {"rm", "-r", "/home/duffaldri/modul2/animal", NULL};
                execv("/usr/bin/rm", argv);
            } else {
                while(wait(&status) > 0);
            }
        }
    }
}
```
Untuk memindahkan file sesuai dengan namanya, kita bisa menggunakan command `find`. Command ini akan mencari file dengan kata tertentu di namanya. Kita menggunakan `-exec` untuk mengeksekusi command lainnya terhadap file yang telah ditemukan itu. File yang tidak masuk ke kategori apapun tidak akan dipindahkan sehingga ketika direktori `animal` dihapus, file tersebut juga akan ikut terhapus.

### (d) Menghapus Gambar Bird
Berikut potongan program untuk bagian ini:
```bash
void removeBird() {
    pid_t child_id = fork();
    
    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }
    // Source: https://stackoverflow.com/questions/2605130/redirecting-exec-output-to-a-buffer-or-file
    if(child_id == 0) {
        char *argv[] = {"find", "/home/duffaldri/modul2/darat", "-iname", "*bird*", "-exec", "rm", "{}", ";", NULL}; 
        execv("/usr/bin/find", argv);
    }
}
```
Tidak jauh berbeda dengan poin sebelumnya, kita hanya perlu melakukan `find` dengan kata kunci _bird_. Setelah itu, file yang ditemukan tinggal di-remove menggunakan `rm`.

Berikut screenshot dari direktori modul2:

![Screenshot modul2](https://imgur.com/fliUOsv.png)

Berikut screenshot dari direktori air:

![Screenshot air](https://imgur.com/p1cgwu4.png)

Berikut screenshot dari direktori darat:

![Screenshot darat](https://imgur.com/w8j1vti.png)


### (e) Membuat File list.txt
Untuk menyelesaikan poin ini, kami melakukan 2 tahap.

Pertama, kami membuat file `temp.txt` yang berisi output dari `ls -l`.
```bash
void ls() {
    pid_t child_id = fork();

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) {
        // char *argv[] = {"sh", "-c", "ls", "-l",  ">", "/home/duffaldri/modul2/air/temp.txt", NULL}; 
        // execv("/bin/sh", argv);

        char *cmd = "ls -l /home/duffaldri/modul2/air > /home/duffaldri/modul2/air/temp.txt";
        execl("/bin/sh", "sh", "-c", cmd, 0);
    } 
}
```
File ini memiliki tampilan sebagai berikut:

![image.png](https://imgur.com/JHOyZNG.png)

Tahap kedua, file `temp.txt` akan di-parsing tiap linenya dan dipindahkan ke dalam file `list.txt`. Setelah selesai, file `temp.txt` akan dihapus.

```bash
void makeList() {
    FILE * fp;
    FILE * fp2;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("/home/duffaldri/modul2/air/temp.txt", "r");
    fp2 = fopen("/home/duffaldri/modul2/air/list.txt", "w+");
    if (fp == NULL || fp2 == NULL)
        exit(EXIT_FAILURE);
    
    while ((read = getline(&line, &len, fp)) != -1) {
        
        if(line[0] == '-') {
            len = strlen(line);
            char format[4] = {'\0'};
            for(int i = 0; i < 3; i++) {
                format[i] = line[len - (4-i)];
            }
            
            if(!(strcmp(format, "png") && strcmp(format, "jpg"))) {
                char p[6] = {'\0'};
                int pFlag = 0;
                int flag = 1;
                
                char username[25] = {'\0'};
                int spaceCounter = 0;

                char animName[25] = {'\0'};
                int animFlag = 0;

                int i = 1, j = 0, k = 0;
                while(line[i] != '\0') {
                    if(line[i] == ' ') {
                        spaceCounter++;
                        while(line[i+1] == ' ') {
                            i++;
                        } 
                    }

                    else if(spaceCounter == 2) {
                        username[j] = line[i];
                        j++;
                    }

                    else if(spaceCounter == 8 && animFlag == 0) {
                        if(line[i] == '_') animFlag = 1;
                        else {
                            animName[k] = line[i];
                            k++;
                            // printf(" %c", line[i]);
                        }
                    }

                    // printf(" %c", line[i]);
                    if(pFlag == 0) {
                        if(line[i] == '-') pFlag = 1;
                        else p[i-1] = line[i];
                    }
                    i++;
                }

                // printf("%s_%s_%s.%s\n", username, p, animName, format);
                fprintf(fp2, "%s_%s_%s.%s\n", username, p, animName, format);
            }

            // printf("%d ", i);
            
        }
    }

    fclose(fp);
    fclose(fp2);
    if (line)
        free(line);

    
    pid_t child3_id = fork();

    if(child3_id < 0) exit(EXIT_FAILURE);

    if(child3_id == 0) {
        char *argv[] = {"rm", "/home/duffaldri/modul2/air/temp.txt", NULL};
        execv("/usr/bin/rm", argv);
    }
}
```
Potongan program di atas mencari UID, UID Permission, nama hewan, dan format file dengan melakukan iterasi tiap karakter di tiap line. Ketiganya akan digabungkan menjadi format yang diminta, yaitu `UID_[UID file permission]_Nama File.[jpg/png]`. Setelah itu, untuk menuliskannya ke dalam `list.txt`, kami menggunakan fungsi `fprintf()`.

Setelah semua tahap selesai, file `temp.txt` akan dihapus menggunakan command `rm`.

Berikut hasil dari fungsi ini:

![Screenshot list.txt](https://imgur.com/hjnMq0J.png)


---
## Soal 1
### Kendala
- Error pada pengalokasian memori array

