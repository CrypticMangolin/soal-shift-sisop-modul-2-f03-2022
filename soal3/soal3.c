#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>

void initDir() {
    pid_t child_id;
    child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) { 
            char *argv[] = {"mkdir", "/home/duffaldri/modul2/darat", NULL};
            execv("/bin/mkdir", argv);
    } else {
        while(wait(&status) > 0);
        pid_t child2_id;
        child2_id = fork();

        if(child2_id == 0) {
            sleep(3);
            char *argv[] = {"mkdir", "/home/duffaldri/modul2/air", NULL}; 
            execv("/bin/mkdir", argv);
        }
    }
}

void moveAnimals() {
    pid_t child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if(child_id == 0) {
            char *argv[] = {"find", "/home/duffaldri/modul2/animal", "-iname", "*air*", "-exec", "cp", "{}", "/home/duffaldri/modul2/air/", ";", NULL};
            execv("/usr/bin/find", argv);
    } 
    else {
        while(wait(&status) > 0);
        pid_t child2_id = fork();
        
        if(child2_id < 0) exit(EXIT_FAILURE);
            
        if(child2_id == 0) {
            char *argv[] = {"find", "/home/duffaldri/modul2/animal", "-iname", "*darat*", "-exec", "cp", "{}", "/home/duffaldri/modul2/darat/", ";", NULL}; // JANGAN LUPA GANTI DIR
            execv("/usr/bin/find", argv);
        } else {
            while(wait(&status) > 0);
            pid_t child3_id = fork();

            if(child3_id < 0) exit(EXIT_FAILURE);

            if(child3_id == 0) {
                char *argv[] = {"rm", "-r", "/home/duffaldri/modul2/animal", NULL};
                execv("/usr/bin/rm", argv);
            } else {
                while(wait(&status) > 0);
            }
        }
    }
}

void unzipAnimal() {
    pid_t child_id = fork();
    int status;
    
    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"unzip", "/home/duffaldri/modul2/animal.zip", "-d", "/home/duffaldri/modul2/", NULL}; 
            execv("/usr/bin/unzip", argv);
    } else {
        while(wait(&status) > 0);
        moveAnimals();
    }  
}

void removeBird() {
    pid_t child_id = fork();
    
    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }
    // Source: https://stackoverflow.com/questions/2605130/redirecting-exec-output-to-a-buffer-or-file
    if(child_id == 0) {
        char *argv[] = {"find", "/home/duffaldri/modul2/darat", "-iname", "*bird*", "-exec", "rm", "{}", ";", NULL}; 
        execv("/usr/bin/find", argv);
    }
}

void ls() {
    pid_t child_id = fork();

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) {
        // char *argv[] = {"sh", "-c", "ls", "-l",  ">", "/home/duffaldri/modul2/air/temp.txt", NULL}; 
        // execv("/bin/sh", argv);

        char *cmd = "ls -l /home/duffaldri/modul2/air > /home/duffaldri/modul2/air/temp.txt";
        execl("/bin/sh", "sh", "-c", cmd, 0);
    } 
}

void makeList() {
    FILE * fp;
    FILE * fp2;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("/home/duffaldri/modul2/air/temp.txt", "r");
    fp2 = fopen("/home/duffaldri/modul2/air/list.txt", "w+");
    if (fp == NULL || fp2 == NULL)
        exit(EXIT_FAILURE);
    
    while ((read = getline(&line, &len, fp)) != -1) {
        
        if(line[0] == '-') {
            len = strlen(line);
            char format[4] = {'\0'};
            for(int i = 0; i < 3; i++) {
                format[i] = line[len - (4-i)];
            }
            
            if(!(strcmp(format, "png") && strcmp(format, "jpg"))) {
                char p[6] = {'\0'};
                int pFlag = 0;
                int flag = 1;
                
                char username[25] = {'\0'};
                int spaceCounter = 0;

                char animName[25] = {'\0'};
                int animFlag = 0;

                int i = 1, j = 0, k = 0;
                while(line[i] != '\0') {
                    if(line[i] == ' ') {
                        spaceCounter++;
                        while(line[i+1] == ' ') {
                            i++;
                        } 
                    }

                    else if(spaceCounter == 2) {
                        username[j] = line[i];
                        j++;
                    }

                    else if(spaceCounter == 8 && animFlag == 0) {
                        if(line[i] == '_') animFlag = 1;
                        else {
                            animName[k] = line[i];
                            k++;
                            // printf(" %c", line[i]);
                        }
                    }

                    // printf(" %c", line[i]);
                    if(pFlag == 0) {
                        if(line[i] == '-') pFlag = 1;
                        else p[i-1] = line[i];
                    }
                    i++;
                }

                // printf("%s_%s_%s.%s\n", username, p, animName, format);
                fprintf(fp2, "%s_%s_%s.%s\n", username, p, animName, format);
            }

            // printf("%d ", i);
            
        }
    }

    fclose(fp);
    fclose(fp2);
    if (line)
        free(line);

    
    pid_t child3_id = fork();

    if(child3_id < 0) exit(EXIT_FAILURE);

    if(child3_id == 0) {
        char *argv[] = {"rm", "/home/duffaldri/modul2/air/temp.txt", NULL};
        execv("/usr/bin/rm", argv);
    }
}

int main() {
    int status;
    initDir();

    while(wait(&status) > 0);
    unzipAnimal();
    while(wait(&status) > 0);
    removeBird();
    while(wait(&status) > 0);
    ls();
    while(wait(&status) > 0);
    makeList();
    
    return 0;   
}
