#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <malloc.h>
#include <wait.h>

typedef struct f {
    int  year;
    char title[50];
} Film;

void MKDir(char *folder) {
    char *argv[] = {"mkdir", "-p", folder, NULL};
    execv("/bin/mkdir", argv);
}
    
void UNZip(char *zipfile, char *folder) {
    char *argv[] = {"unzip", zipfile, "-d", folder, NULL};
    execv("/bin/unzip", argv);
}

void RM_r(char *filedir) {
    char *argv[] = {"rm", "-r", filedir, NULL};
    execv("/bin/rm", argv);
}

void CPY(char *srcFile, char *Destination) {
    char *argv[] = {"cp", srcFile, Destination, NULL};
    execv("/bin/cp", argv);
}

size_t file_list(const char *path, char ***ls, int mode) {
    // Array file listing
    size_t count = 0;
    DIR *dp = NULL;
    struct dirent *ep = NULL;

    dp = opendir(path);
    if(NULL == dp) {
        fprintf(stderr, "no such directory: '%s'", path);
        return 0;
    }

    *ls = NULL;
    ep = readdir(dp);
    while(NULL != ep){
        count++;
        ep = readdir(dp);
    }

    rewinddir(dp);
    *ls = calloc(count, sizeof(char *));

    count = 0;
    ep = readdir(dp);
    while(NULL != ep) {
        // Skip . and ..
        if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
            if (mode == 1) {
                (*ls)[count++] = strdup(ep->d_name);
            } else {
                if (strcmp(strrchr(ep->d_name, '\0') - 4, ".png") == 0) {
                    (*ls)[count++] = strdup(ep->d_name);
                } else {
                    // REMOVE DIR
                    if (fork() == 0) {
                        char filedir[50] = "drakor/";
                        strcat(filedir, ep->d_name);
                        RM_r(filedir);
                    }
                }
            }
        }
        ep = readdir(dp);
    }

    closedir(dp);
    return count;
}

size_t strSplit (char *str, char *delim, char ***array) {
    size_t i = 0;  
    char **tempArr = NULL;
    char * token = strtok(str, delim);
    while( token != NULL ) {
        tempArr = (char **)realloc(tempArr, (i + 1) * sizeof(char *));
        tempArr[i++] = token;
        token = strtok(NULL, delim);
    }
    *array = tempArr;
    return i;
}

int comparator(const void *a, const void *b) 
{
    if (((Film *)a)->year == ((Film *)b)->year) {
        return strcmp(((Film *)a)->title, ((Film *)b)->title);
    }
    return (((Film *)a)->year-((Film *)b)->year);
}

int main() {
    char FolderDir[] = "/home/nao/drakor";
    int status_Unzip;
    pid_t child_unzip = fork();

    if (child_unzip == 0) {
    	int status_mkdir;
    	pid_t child_mkdir = fork();

        if (child_mkdir==0) MKDir(FolderDir);
        else {
            while ((wait(&status_mkdir)) > 0);
            UNZip("drakor.zip", FolderDir);
        }
    } else {
        while ((wait(&status_Unzip)) > 0);
        
        int statusPP;
        pid_t child_PosterProcess = fork();

        if(child_PosterProcess == 0) {
            char **filelist;
            size_t fcount;
            fcount = file_list(FolderDir, &filelist, 0);
            
            for(size_t i=0; i<fcount; i++) {            
                char curFile[60];
                strcpy(curFile, filelist[i]);

                filelist[i][strlen(filelist[i])-4]='\0';

                char **PosterArr = NULL;
                size_t PArrSize = strSplit(filelist[i], "_", &PosterArr);
                
                int statusGmk;
                pid_t child_G_mkdir;
                for (size_t j=0; j<PArrSize; j++) {                
                    char temp[50];
                    strcpy(temp,PosterArr[j]);
                    char **Poster = NULL;
                    size_t PSize = strSplit(temp, ";", &Poster);
                    
                    char genreDir[100];
                    sprintf(genreDir, "%s/%s", FolderDir, Poster[2]);
                    
                    child_G_mkdir = fork();                
        
                    if (child_G_mkdir==0) {
                        MKDir(genreDir);
                    } else {
                        while ((wait(&statusGmk)) > 0);
                        pid_t child_cp = fork();

                        if (child_cp==0) {
                        char src[100], dst[100];
                            sprintf(src, "%s/%s", FolderDir, curFile);
                            sprintf(dst, "%s/%s/%s.png", FolderDir, Poster[2], Poster[0]);
                            CPY(src, dst);
                        }
                    }
                    FILE* printPtr, *sizePtr;
                    char datafile[100];
                    sprintf(datafile, "%s/%s/data.txt", FolderDir, Poster[2]);                
                    printPtr = fopen(datafile, "a");
                    sizePtr = fopen(datafile, "r+");
                    int pcount;
                    if(fscanf(sizePtr, "%d", &pcount)==1) {
                        rewind(sizePtr); pcount++;
                        fprintf(sizePtr, "%d\n", pcount);
                    } else {
                        fprintf(sizePtr, "1\n");
                    }             
                    fprintf(printPtr, "%s %s\n", Poster[1], Poster[0]);
                    fclose(sizePtr); fclose(printPtr);
                }
                
                while ((wait(&statusGmk)) > 0);
                if (fork()==0) {
                    char a[100];
                    sprintf(a, "%s/%s", FolderDir, curFile);
                    RM_r(a);
                }                      
            } // end processing of all poster
        } else {
            // Sorting isi data.txt
            sleep(1);
            while ((wait(&statusPP)) > 0);
            char **folderlist;
            size_t count;
            
            count = file_list(FolderDir, &folderlist, 1);
            for (int i=0; i<count; i++) {
                int n;
                char datafile[100];
                sprintf(datafile, "%s/%s/data.txt", FolderDir, folderlist[i]);
                FILE* f = fopen(datafile, "r+");
                        
                // printf("%s\n", datafile);
                // if(f==NULL) {printf("\n\nrip file\n\n"); break;}
                
                fscanf(f, "%d ", &n);
                Film Poster[n];
                for (int j=0; j<n; j++) {
                    fscanf(f, "%d %s ", &Poster[j].year, Poster[j].title);
                }
                
                qsort(Poster, n, sizeof(Film), comparator);
                rewind(f);
                
                fprintf(f, "Kategori : %s", folderlist[i]);
                for (int j=0; j<n; j++) {
                    fprintf(f, "\n\nNama : %s\nRilis Tahun : %d", Poster[j].title, Poster[j].year);
                }            
                fclose(f);
            }
        }
    }
    return 0;
}
