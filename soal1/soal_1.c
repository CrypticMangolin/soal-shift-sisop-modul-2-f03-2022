#include <json-c/json.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <malloc.h>
#include <wait.h>
#include <time.h>

typedef struct g {
    int rarity;
    char name[20];
}gacha;

void MKDir(char *folder) {
    char *argv[] = {"mkdir", "-p", folder, NULL};
    execv("/bin/mkdir", argv);
}

void DL_Unzip(char *link, char *zipname, char *folder) {
    int status;
    pid_t child = fork();
    if (child < 0) {
        exit(EXIT_FAILURE); 
    }
    if (child == 0) {
        char *argv[] = {"wget", "-q", link, "-O", zipname, NULL};
        execv("/bin/wget", argv);
    } else {
        while ((wait(&status)) > 0);      
        char *argv[] = {"unzip", "-q", "-n", zipname, "-d", folder, NULL};
        execv("/bin/unzip", argv);
    }
}

void Zip_RM(char *pass, char *zipname, char *files) {
    int status;
    pid_t child = fork();
    if (child < 0) {
        exit(EXIT_FAILURE); 
    }
    if (child == 0) {
        char *argv[] = {"zip", "-r", "-P", pass, zipname, files, NULL};
        execv("/bin/zip", argv);
    } else {
        while ((wait(&status)) > 0);      
        char *argv[] = {"rm", "-r", files, NULL};
        execv("/bin/rm", argv);
    }
}

void getGacha (char* Path, gacha* temp) {
  FILE *f;
  size_t size = 4096;
  char buffer[size];

  struct json_object *parsed_json, *name, *rarity;
  f = fopen(Path, "r");
	fread(buffer, size, 1, f);
	fclose(f);

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

	strcpy(temp->name, json_object_get_string(name));
	temp->rarity = json_object_get_int(rarity);
}

size_t loadDB (char* dbPath, gacha** DB) {
    size_t count = 0;
    DIR *dp = opendir(dbPath);
    struct dirent *ep = NULL;

    if(NULL == dp) {
        perror ("Couldn't open the directory");
    }

    while ((ep=readdir(dp)) != NULL) {
        if (strcmp(strrchr(ep->d_name,'\0')-5,".json"))
            count++;
    }

    rewinddir(dp);
    gacha *Arr = (gacha*) calloc(count, sizeof(gacha*));

    char filepath[200];
    while ((ep=readdir(dp)) != NULL) {
        if (strcmp(strrchr(ep->d_name,'\0')-5,".json")) {
            sprintf(filepath, "%s/%s", dbPath, strdup(ep->d_name));
            getGacha(filepath, &Arr[count++]);
        }
    }

    *DB = Arr;
    closedir(dp);
    return count;
}

void DoGacha() {
    if ((chdir("gacha_gacha")) < 0)
        exit(EXIT_FAILURE);

    struct tm* t;
    time_t curtime;
    FILE *f;
    int primogems = 79000, count = 0;
    
    char folder[20], file[200];
    gacha *weapons, *characters, temp;
    size_t wcount = loadDB("weapons", &weapons);
    size_t ccount = loadDB("characters", &characters);

    while (primogems >= 160) {
        if (count%90 == 0) {
            int status;
            pid_t child = fork();

            if(child < 0) exit(EXIT_FAILURE);
            if(child == 0) {
                sprintf(folder, "total_gacha%d", count+90);
                MKDir(folder);
            } else {
                while ((wait(&status)) > 0);
            }
        }
        if (count%10 == 0) {
            if (count) fclose(f);
            sleep(1);
            curtime = time(NULL);
            t = localtime(&curtime);
            sprintf(file, "%s/%02d:%02d:%02d_gacha_%d", folder, t->tm_hour, t->tm_min, t->tm_sec, count+10);
            f = fopen(file, "w+");
        }

        primogems-=160; count++;
        if(count%2==0) {
            temp = weapons[rand()%wcount];
            fprintf(f, "%d_weapons_%s_%d_%d", count, temp.name, temp.rarity, primogems);
        } else {
            temp = characters[rand()%ccount];
            fprintf(f, "%d_characters_%s_%d_%d", count, temp.name, temp.rarity, primogems);
        }
    }
    fclose(f);

    if ((chdir("..")) < 0)
        exit(EXIT_FAILURE);
}

void PrepareDB (char *folder) {
    char *linkweapon = "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";
    char *linkchara  = "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";
    char *dbweapon   = "dbweapon.zip";
    char *dbchara    = "dbchara.zip";
    int status;

    if (fork()==0) {
        if (fork()==0) {
            DL_Unzip(linkchara, dbchara, folder);
        } else {
            DL_Unzip(linkweapon, dbweapon, folder);
        }
    }

    while(wait(&status)>0);
    return;
}

int main() {
    srand((unsigned) time(NULL));

    pid_t pid, sid;
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir("/home/nao")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    char *gachafolder = "/home/nao/gacha_gacha";

    PrepareDB(gachafolder);   

    while (1) {
        struct tm* t;
        time_t curtime = time(NULL);
        t = localtime(&curtime);
        
        if(t->tm_mday == 30 && t->tm_mon == 2 && t->tm_hour == 4 && t->tm_min == 44) {
            DoGacha();
        }
        else if(t->tm_mday == 30 && t->tm_mon == 2 && t->tm_hour == 7 && t->tm_min == 44) {
            Zip_RM("satuduatiga", "NotSafeForWibu.zip", gachafolder);
        }

        sleep(30);
    }
}
